<?php

class Sum {
	
	/**
	* variable that stores the list of numbers
	**/
	public $numberList;
	public $sum = 0;
	
	public function __construct($numberList)
    {
        $this->numberList = $numberList;
    }
	
	/**
	* method that sends zeros to the end of the list
	**/
	public function execute()
    {
		$this->sumNumbers($this->numberList);
        return $this->sum;
    }
	
	/**
	* method that sum numbers
	***/
	public function sumNumbers($numbers) 
	{
		foreach ($numbers as $number){
			if (!is_array($this->numberList)) {
				$this->sum += $number;
			}else{
				$this->sumNumbers($numbers);
			}				
		}
	}
}

// Algorithm usage sample code
$numberList = array (
    1,
    2,
    5,
    8,
    array(2,3,array(1,2))
   );
$action = new Sum($numberList);

echo('<pre>');

print_r($numberList);

echo "<br>***RESULT***<br><br>";
print_r($action->execute());

echo('</pre>');