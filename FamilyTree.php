<?php

class FamilyTree {
	
	/**
	* variable that stores the family tree
	**/
	public $data;
	
	public function __construct($data)
    {
        $this->data = $data;
    }
	
	/**
	* method that sort descending the family tree
	**/
	public function sortDescending()
    {
		foreach ($this->data as $key => $person) {
			$aux[$key] = $person['padre'];
		}
		
		array_multisort($aux, SORT_ASC, $this->data);
		
        return $this->data;
    }
}

// Algorithm usage sample code
$data = [
	[
		"registro" => 1,
		"nombre" => "Carlos",
		"padre" => 0
	],
	[
		"registro" => 2,
		"nombre" => "Juan",
		"padre" => 0
	],
	[
		"registro" => 3,
		"nombre" => "Maria",
		"padre" => 1
	],
	[
		"registro" => 4,
		"nombre" => "Carlos 1",
		"padre" => 3
	],
	[
		"registro" => 5,
		"nombre" => "Sofia",
		"padre" => 4
	],
	[
		"registro" => 6,
		"nombre" => "Hernan",
		"padre" => 1
	],
	[
		"registro" => 7,
		"nombre" => "Wilson",
		"padre" => 2
	],
	[
		"registro" => 8,
		"nombre" => "Diana",
		"padre" => 2
	],
	[
		"registro" => 9,
		"nombre" => "Mateo",
		"padre" => 7
	],
	[
		"registro" => 10,
		"nombre" => "Efrain",
		"padre" => 6
	],
];
$FamilyTree = new FamilyTree($data);

echo('<pre>');

print_r($data);
print_r($FamilyTree->sortDescending());

echo('</pre>');